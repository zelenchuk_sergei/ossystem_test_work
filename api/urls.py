from django.urls import path

from . import views

urlpatterns = [
    path('new-item/', views.CreateItem.as_view()),
    path('all-items/', views.ListItems.as_view()),
    path('update-item/', views.UpdateItem.as_view()),
    path('delete-item/', views.DeleteItem.as_view()),
]