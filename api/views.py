from rest_framework import generics
from . import serializers, models


class CreateItem(generics.CreateAPIView):
    serializer_class = serializers.ItemsSerializer


class ListItems(generics.ListAPIView):
    queryset = models.Items.objects.all()
    serializer_class = serializers.ItemsSerializer


class UpdateItem(generics.UpdateAPIView):
    serializer_class = serializers.ItemsSerializer


class DeleteItem(generics.DestroyAPIView):
    serializer_class = serializers.ItemsSerializer
