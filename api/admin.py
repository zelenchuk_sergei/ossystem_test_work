from django.contrib import admin
from . import models


@admin.register(models.CategoryForItems)
class AdminCategory(admin.ModelAdmin):
    list_display = ['title', ]


@admin.register(models.Items)
class AdminItems(admin.ModelAdmin):
    list_display = ['title', 'description', 'category', ]
    list_filter = ['title', ]
