from django.db import models


class CategoryForItems(models.Model):
    title = models.CharField('Category name', max_length=255)

    class Meta:
        ordering = ['pk']
        verbose_name = "Category name"
        verbose_name_plural = "Categories name"

    def __str__(self):
        return self.title


class Items(models.Model):
    category = models.ForeignKey(CategoryForItems, on_delete=models.CASCADE)
    title = models.CharField('Title', max_length=255)
    description = models.TextField('Description')
    image = models.ImageField('Image', upload_to="items_img/%Y/%m/%d/", blank=True, null=True)

    class Meta:
        ordering = ['title']
        verbose_name = "Item"
        verbose_name_plural = 'Items'

    def __str__(self):
        return self.title
