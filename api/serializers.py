from rest_framework import serializers
from . import models


class ItemsSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            'id',
            'title',
            'category',
            'description',
            'image',
        )
        model = models.Items
