# ossystem_test_work

## About
test work for OS system company

## Introduction to the test app:

This is a test we give to everyone we hire to test some skills which are required to work on our
Python projects.

Your test is to create backend Python application based on two simple views: form to input data
and grid to navigate stored data:
1. Form has 4 fields:
a. image
b. category
c. title
d. description
2. Grid should have:
                           a.  actions to create, edit or delete records
                            b.  filter field to search records by title
                  c.  optional - add g